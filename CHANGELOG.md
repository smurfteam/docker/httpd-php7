# changelog for the smurfteam/docker/httpd-php7's project

## [0.6] - 2020-10-15
### Adds
- add builder script

### Changes
- update to Alpine 3.12 Alpine, Apache 2.4.46 and PHP 7.3

## [0.5] - 2019-03-05
### Adds
- add other Symfony requirements for PHP: tokenizer, simplexml and iconv

## [0.4] - 2019-02-01
### Adds
- add MySQL extensions (mysqli + pdo) for PHP

## [0.3] - 2019-01-26
### Adds
- add new extensions for PHP7

## [0.2] - 2019-01-24
### Changes
- bugfix for create home working subdirectory
- documentation correction

## [0.1] - 2018-08-31
### Adds
- start php7-fpm process
- start httpd process
- connection between httpd and php7
- externalization of datas and configurations files
- initial project
