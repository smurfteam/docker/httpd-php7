# HEADERS
FROM alpine:3.12
LABEL maintainer "bl@cksmurf.net"
LABEL version "master"
LABEL name "httpd-php7"
LABEL vendor "BlackSmurf"
LABEL license "CC BY-NC-ND 4.0"

# FOR THE BUILD!
ARG install_home="/smurf"
ARG with_samba=0
ENV env_samba=${with_samba}
ENV PATH "${PATH}:${install_home}/bin"

# INSTALL DEFAULT PACKAGES
RUN apk update && apk upgrade

RUN apk add --update bash openrc

RUN apk add apache2 \
		apache2-ssl \
		apache2-ldap \
		apache2-proxy \
		apache2-http2

RUN apk add php7-zip \
		php7-fpm \
		php7-session \
		php7-phar \
		php7-json \
		php7-mbstring \
		php7-openssl \
		php7-pdo \
		php7-pdo_mysql \
		php7-mysqli \
		php7-ldap \
		php7-curl \
		php7-xml \
		php7-ctype \
		php7-dom \
		php7-tokenizer \
		php7-simplexml \
		php7-iconv \
		php7-cli

# FOR WINDOWS HOST
RUN if [ ${env_samba} -eq 1 ]; then apk add samba shadow && \
    rm -rf /var/cache/apk/* && \
    adduser -D -G users -H -S -g 'Samba User' -h /tmp smbuser && \
    file="/etc/samba/smb.conf" && \
    sed -i 's|^;* *\(log file = \).*|   \1/dev/stdout|' $file && \
    sed -i 's|^;* *\(load printers = \).*|   \1no|' $file && \
    sed -i 's|^;* *\(printcap name = \).*|   \1/dev/null|' $file && \
    sed -i 's|^;* *\(printing = \).*|   \1bsd|' $file && \
    sed -i 's|^;* *\(unix password sync = \).*|   \1no|' $file && \
    sed -i 's|^;* *\(preserve case = \).*|   \1yes|' $file && \
    sed -i 's|^;* *\(short preserve case = \).*|   \1yes|' $file && \
    sed -i 's|^;* *\(default case = \).*|   \1lower|' $file && \
    sed -i '/Share Definitions/,$d' $file && \
    echo '   pam password change = yes' >>$file && \
    echo '   map to guest = bad user' >>$file && \
    echo '   usershare allow guests = yes' >>$file && \
    echo '   create mask = 0664' >>$file && \
    echo '   force create mode = 0664' >>$file && \
    echo '   directory mask = 0775' >>$file && \
    echo '   force directory mode = 0775' >>$file && \
    echo '   force user = smbuser' >>$file && \
    echo '   force group = users' >>$file && \
    echo '   follow symlinks = yes' >>$file && \
    echo '   load printers = no' >>$file && \
    echo '   printing = bsd' >>$file && \
    echo '   printcap name = /dev/null' >>$file && \
    echo '   disable spoolss = yes' >>$file && \
    echo '   socket options = TCP_NODELAY' >>$file && \
    echo '   strict locking = no' >>$file && \
    echo '   vfs objects = acl_xattr catia fruit recycle streams_xattr' \
                >>$file && \
    echo '   recycle:keeptree = yes' >>$file && \
    echo '   recycle:versions = yes' >>$file && \
    echo '' >>$file && \
    echo '   # Security' >>$file && \
    echo '   client ipc max protocol = default' >>$file && \
    echo '   client max protocol = default' >>$file && \
    echo '   server max protocol = SMB3' >>$file && \
    echo '   client ipc min protocol = default' >>$file && \
    echo '   client min protocol = CORE' >>$file && \
    echo '   server min protocol = SMB2' >>$file && \
    echo '' >>$file && \
    echo '   # Time Machine' >>$file && \
    echo '   durable handles = yes' >>$file && \
    echo '   kernel oplocks = no' >>$file && \
    echo '   kernel share modes = no' >>$file && \
    echo '   posix locking = no' >>$file && \
    echo '   fruit:aapl = yes' >>$file && \
    echo '   fruit:advertise_fullsync = true' >>$file && \
    echo '   fruit:time machine = yes' >>$file && \
    echo '   smb2 leases = yes' >>$file && \
    echo '' >>$file && \
    rm -rf /tmp/*; fi

# BUGFIXES!
RUN mkdir /run/openrc && touch /run/openrc/softlevel && ln -s /usr/lib/libxml2.so.2 /usr/lib/libxml2.so

# PREPARE THE IMAGE
RUN mkdir -p "${install_home}"
WORKDIR ${install_home}
RUN mkdir bin && mkdir etc && mkdir etc/apache2 && mkdir etc/php7 && mkdir logs

COPY etc/php7/php.ini etc/php7/php.ini
COPY bin/exploit.sh bin/exploit.sh
COPY bin/samba.sh bin/samba.sh
RUN chmod +x bin/exploit.sh bin/samba.sh

# INSTALL AND TEMPLATING CONFIGURATIONS FILES
COPY etc/apache2/httpd.conf.tmpl etc/apache2/httpd.conf.tmpl
COPY etc/php7/www.conf.tmpl etc/php7/www.conf.tmpl
COPY etc/php7/php-fpm.conf.tmpl etc/php7/php-fpm.conf.tmpl
COPY etc/exploit.cfg.tmpl etc/exploit.cfg.tmpl
RUN sed "s|\$HOME|${install_home}|g" etc/apache2/httpd.conf.tmpl > etc/apache2/httpd.conf && rm -f etc/apache2/httpd.conf.tmpl
RUN sed "s|\$HOME|${install_home}|g" etc/php7/www.conf.tmpl > etc/php7/www.conf && rm -f etc/php7/www.conf.tmpl
RUN sed "s|\$HOME|${install_home}|g" etc/php7/php-fpm.conf.tmpl > etc/php7/php-fpm.conf && rm -f etc/php7/php-fpm.conf.tmpl
RUN sed "s|\$HOME|${install_home}|g" etc/exploit.cfg.tmpl > etc/exploit.cfg && rm -f etc/exploit.cfg.tmpl

# EXPOSE
VOLUME [ "${install_home}/apache2", "${install_home}/php7" ]
EXPOSE 80 443
EXPOSE 137/udp 138/udp 139 445

# FOR THE CONTAINER!
CMD bin/exploit.sh start ${env_samba}
