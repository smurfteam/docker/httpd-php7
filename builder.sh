#!/bin/bash
#@name build.sh
#@description builder program
#@author Julien Buonocore <julien.buonocore@ac-aix-marseille.fr>
#@license_
# This script and the entire contents of this program are protected by copyright
# Also, this work is licensed under the Creative Commons
# Attribution-NonCommercial-NoDeratives 4.0 International License (CC BY-NC-ND 4.0)
# To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/deed.us
#@_license
#@version 0.1
VERSION="0.1"

function _showSyntax() {
    echo "Syntax: $0 <command>"
    echo
    echo "List of commands"
    echo "  clean: clean all docker httpd-php7 images and his containers"
    echo
    return 1
}

function _clean() {
    $(which docker) rm --force $(docker ps -a | grep "httpd-php7" | awk '{print $1}') &>/dev/null
    $(which docker) rmi --force $(docker images | grep "httpd-php7" | awk '{print $3}') &>/dev/null
    $(which docker) rmi --force $(docker images | grep "alpine" | awk '{print $3}') &>/dev/null
}

function _build() {
    $(which docker) build -t httpd-php7 .
}

function _run() {
    $(which docker) run --detach --restart always --publish 80:80 --publish 443:443 httpd-php7
}

#@description main program
#@published
function main() {
    echo "Builder program"
    echo "version ${VERSION}"
    echo

    case "$1" in
        "clean")
            _clean $@
             RETVAL=$?
             ;;
             
        "build")
            _build $@
             RETVAL=$?
             ;;

        "run")
            _run $@
             RETVAL=$?
             ;;

        *)
            _showSyntax
    
    esac

    return ${RETVAL}

    _showSyntax      
}

# main
main $@