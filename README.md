# Simply and optimized HTTPD Worker with PHP-FPM 7.x
* from: alpine linux
* author: Julien Buonocore <julien.buonocore@gmail.com>
* docker's repository: blacksmurf/httpd-php7

## Deployment

### Linux install procedure

#### build your image (optional)
    docker build -t httpd-php7 .

### test your image
    docker run -ti --rm -p 80:80 http-php7

#### instantiate a container from our image
    docker run \
    --detach \
    --restart always \
    --publish 80:80 \
    --publish 443:443 \
    --volume <host_install_directory>/apache2:<container_install_directory>/apache2 \
    --volume <host_install_directory>/php7:<container_install_directory>/php7 \
    --name <your_container_name> \
    httpd-php7

### Windows install procedure (for development platform and Docker Desktop only)

#### build image on your desktop (necessary)
    docker build -t httpd-php7 --build-arg install_home=<container_install_directory> --build-arg with_samba=1 .

#### create local named volumes
    docker volume create apache2
    docker volume create php7

#### create local network 192.168.100.0/24
    docker network create --subnet=192.168.100.0/24 localnetwork

#### instanciate a container with previous volumes, network and your desktop image
    docker run --detach --net=localnetwork --ip=192.168.100.2 -e USERID=0 -e GROUPID=0 --mount source=apache2,target=<container_install_directory>/apache2 --mount source=php7,target=<container_install_directory>/php7 --name <your_container_name> httpd-php7

#### activate the TCP route with this command line and cmd.exe tool (require administrator rights) :
    route add 192.168.100.0 mask 255.255.255.0 <docker_desktop_ip_gateway> -p

## Exploitation

### Connect to the container in real-time
    docker exec -it <your_container_name> /bin/bash

### Remove all unused data (dangling images, stopped containers)
    docker system prune

### Remove unused volumes
    docker volume prune

### Remove unused networks
    docker network prune

### Access shared working folders on Windows
    \\192.169.100.2

### Connect to Hyper-V MobyLinux (for Docker for Windows)
    docker run --privileged -it --rm -v /var/run/docker.sock:/var/run/docker.sock jongallant/ubuntu-docker-client
    docker run --net=host --ipc=host --uts=host --pid=host -it --security-opt=seccomp=unconfined --privileged --rm -v /:/host alpine /bin/sh
    chroot /host